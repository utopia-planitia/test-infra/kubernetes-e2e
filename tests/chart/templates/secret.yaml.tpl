apiVersion: v1
kind: Secret
metadata:
  name: "helmfile-test-scripts-e2e"
type: Opaque
stringData:
{{ (tpl (.Files.Glob "test-scripts/*").AsConfig . ) | indent 2 }}