apiVersion: v1
kind: Pod
metadata:
  name: "port-forward-test"
  annotations:
    helm.sh/hook: test-success
spec:
  serviceAccountName: testing-account-e2e
  containers:
    - name: test
      image: "k8s.gcr.io/conformance:{{ .Capabilities.KubeVersion.Version }}"
      command:
        - bash
        - -c
        - bash /tests/port-forward.sh || bash /tests/port-forward.sh
      resources:
        requests:
          memory: "8Gi"
          cpu: "4"
        limits:
          memory: "8Gi"
          cpu: "4"
      volumeMounts:
        - name: test-scripts
          mountPath: "/tests"
          readOnly: true
  volumes:
    - name: test-scripts
      secret:
        secretName: "helmfile-test-scripts-e2e"
  restartPolicy: Never