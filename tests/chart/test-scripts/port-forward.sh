#!/bin/bash
set -euxo pipefail

sed -i 's/"--p"/"--nodes=4"/g' /run_e2e.sh

#port-forward
E2E_PARALLEL=y
E2E_SKIP="Alpha|\[(Disruptive|Feature:[^\]]+|Flaky)\]"
E2E_FOCUS="port-forward"
/run_e2e.sh
