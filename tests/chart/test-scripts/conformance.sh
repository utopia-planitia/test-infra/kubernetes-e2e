#!/bin/bash
set -euxo pipefail

sed -i 's/"--p"/"--nodes=4"/g' /run_e2e.sh

#conformance
E2E_PARALLEL=y
E2E_SKIP="Alpha|Kubectl|Serial|\[(Disruptive|Feature:[^\]]+|Flaky)\]"
E2E_FOCUS="\[Conformance\]"
/run_e2e.sh
