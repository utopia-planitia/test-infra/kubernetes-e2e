#!/bin/bash
set -euxo pipefail

# serial
E2E_PARALLEL=n
E2E_SKIP="Alpha|Kubectl|\[(Disruptive|Feature:[^\]]+|Flaky)\]"
E2E_FOCUS="Serial.*Conformance"
/run_e2e.sh
